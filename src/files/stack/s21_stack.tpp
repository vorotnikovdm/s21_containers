#ifndef S21_STACK_TPP
#define S21_STACK_TPP

namespace s21 {

template <typename T>
stack<T>::stack() : top_(nullptr), size_(0) {}

template <typename T>
stack<T>::stack(std::initializer_list<value_type> const &items)
    : top_(nullptr), size_(0) {
  for (const auto &item : items) {
    push(item);
  }
}

template <typename T>
stack<T>::stack(const stack &s) : top_(nullptr), size_(0) {
  Node *current = s.top_;
  while (current != nullptr) {
    push(current->data);
    current = current->next;
  }
}

template <typename T>
stack<T>::stack(stack &&s) : top_(s.top_), size_(s.size_) {
  s.top_ = nullptr;
  s.size_ = 0;
}

template <typename T>
stack<T>::~stack() {
  while (!empty()) pop();
}

template <typename T>
stack<T> stack<T>::operator=(stack &&s) {
  while (!empty()) pop();
  top_ = s.top_;
  size_ = s.size_;
  s.top_ = nullptr;
  s.size_ = 0;
  return *this;
}

template <typename T>
typename stack<T>::const_reference stack<T>::top() {
  if (empty()) throw std::out_of_range("stack is empty");
  return top_->data;
}

template <typename T>
bool stack<T>::empty() {
  return size_ == 0;
}

template <typename T>
typename stack<T>::size_type stack<T>::size() {
  return size_;
}

template <typename T>
void stack<T>::push(const_reference value) {
  if (top_ == nullptr) {
    top_ = new Node(value);
  } else {
    Node *i = top_;
    while (i->next != nullptr) i = i->next;
    i->next = new Node(value);
  }
  ++size_;
}

template <typename T>
void stack<T>::pop() {
  if (empty()) throw std::out_of_range("stack is empty");
  if (top_->next == nullptr) {
    delete top_;
  } else {
    Node *last = top_;
    Node *prev = top_;
    while (last->next != nullptr) {
      prev = last;
      last = last->next;
    }
    prev->next = nullptr;
    delete last;
  }
  --size_;
}

template <typename T>
void stack<T>::swap(stack &other) {
  std::swap(top_, other.top_);
  std::swap(size_, other.size_);
}

template <typename T>
template <typename... Args>
void stack<T>::insert_many_front(Args &&... args) {
  for (const auto &arg : {args...}) {
    push(arg);
  }
}

};  // namespace s21

#endif
