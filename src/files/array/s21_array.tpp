#ifndef S21_CONTAINERS_ARRAY_TPP
#define S21_CONTAINERS_ARRAY_TPP

namespace s21 {

// functions

template <typename T, size_t N>
array<T, N>::array() : size_(N) {}

template <typename T, size_t N>
array<T, N>::array(std::initializer_list<value_type> const &items)
    : size_(items.size()) {
  size_t i = 0;
  for (const auto &item : items) {
    data_[i++] = item;
  }
}

template <typename T, size_t N>
array<T, N>::array(const array &a) : size_(a.size_) {
  std::copy(a.data_, a.data_ + N, data_);
}

template <typename T, size_t N>
array<T, N>::array(array &&a) : size_(a.size_) {
  std::move(a.data_, a.data_ + N, data_);
  a.size_ = 0;
}

template <typename T, size_t N>
array<T, N>::~array() {}

template <typename T, size_t N>
array<T, N> &array<T, N>::operator=(array &&a) {
  if (this != &a) {
    std::swap(data_, a.data_);
    std::swap(size_, a.size_);
  }
  return *this;
}

// element access

template <typename T, size_t N>
typename array<T, N>::reference array<T, N>::at(size_type pos) {
  if (pos >= size_) {
    throw std::out_of_range("Index out of range");
  }
  return data_[pos];
}

template <typename T, size_t N>
typename array<T, N>::reference array<T, N>::operator[](size_type pos) {
  return data_[pos];
}

template <typename T, size_t N>
typename array<T, N>::const_reference array<T, N>::front() {
  return data_[0];
}

template <typename T, size_t N>
typename array<T, N>::const_reference array<T, N>::back() {
  return data_[size_ - 1];
}

template <typename T, size_t N>
typename array<T, N>::iterator array<T, N>::data() {
  return data_;
}

// iterators

template <typename T, size_t N>
typename array<T, N>::iterator array<T, N>::begin() {
  return &data_[0];
}

template <typename T, size_t N>
typename array<T, N>::iterator array<T, N>::end() {
  return &data_[size_];
}

// capacity

template <typename T, size_t N>
bool array<T, N>::empty() {
  return size_ == 0;
}

template <typename T, size_t N>
typename array<T, N>::size_type array<T, N>::size() {
  return size_;
}

template <typename T, size_t N>
typename array<T, N>::size_type array<T, N>::max_size() {
  return N;
}

// modifiers

template <typename T, size_t N>
void array<T, N>::swap(array &other) {
  for (size_type i = 0; i < N; ++i) {
    std::swap(data_[i], other.data_[i]);
  }
}

template <typename T, size_t N>
void array<T, N>::fill(const_reference value) {
  for (size_type i = 0; i < N; ++i) {
    data_[i] = value;
  }
}

};  // namespace s21

#endif
