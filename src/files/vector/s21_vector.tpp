#ifndef S21_CONTAINERS_VECTOR_TPP
#define S21_CONTAINERS_VECTOR_TPP

namespace s21 {

// functions

template <typename T>
vector<T>::vector() : data_(nullptr), size_(0), capacity_(0) {}

template <typename T>
vector<T>::vector(size_type n)
    : data_(new value_type[n]), size_(n), capacity_(n) {
  std::fill(data_, data_ + n, value_type{});
}

template <typename T>
vector<T>::vector(std::initializer_list<value_type> const &items)
    : data_(new value_type[items.size()]),
      size_(items.size()),
      capacity_(items.size()) {
  std::copy(items.begin(), items.end(), data_);
}

template <typename T>
vector<T>::vector(const vector &v)
    : data_(new value_type[v.size_]), size_(v.size_), capacity_(v.size_) {
  std::copy(v.data_, v.data_ + v.size_, data_);
}

template <typename T>
vector<T>::vector(vector &&v) noexcept
    : data_(v.data_), size_(v.size_), capacity_(v.capacity_) {
  v.data_ = nullptr, v.size_ = 0, v.capacity_ = 0;
}

template <typename T>
vector<T>::~vector() {
  delete[] data_;
  data_ = nullptr;
  size_ = 0;
  capacity_ = 0;
}

template <typename T>
vector<T> &vector<T>::operator=(vector &&v) noexcept {
  if (this != &v) {
    delete[] data_;
    data_ = v.data_;
    size_ = v.size_;
    capacity_ = v.capacity_;

    v.data_ = nullptr;
    v.size_ = 0;
    v.capacity_ = 0;
  }
  return *this;
}

// element access

template <typename T>
typename vector<T>::reference vector<T>::at(size_type pos) {
  if (pos >= size_) throw std::out_of_range("Index out of range");
  return data_[pos];
}

template <typename T>
typename vector<T>::reference vector<T>::operator[](size_type pos) {
  if (pos >= size_) throw std::out_of_range("Index out of range");
  return data_[pos];
}

template <typename T>
typename vector<T>::const_reference vector<T>::front() {
  if (size_ == 0) throw std::out_of_range("Vector is empty");
  return data_[0];
}

template <typename T>
typename vector<T>::const_reference vector<T>::back() {
  if (size_ == 0) throw std::out_of_range("Vector is empty");
  return data_[size_ - 1];
}

template <typename T>
T *vector<T>::data() {
  return data_;
}

// iterators

template <typename T>
typename vector<T>::iterator vector<T>::begin() {
  return data_;
}

template <typename T>
typename vector<T>::iterator vector<T>::end() {
  return data_ + size_;
}

// capacity

template <typename T>
bool vector<T>::empty() {
  return size_ == 0;
}

template <typename T>
typename vector<T>::size_type vector<T>::size() {
  return size_;
}

template <typename T>
typename vector<T>::size_type vector<T>::max_size() {
  return std::numeric_limits<size_type>::max() / sizeof(value_type);
}

template <typename T>
void vector<T>::reserve(size_type size) {
  if (size > max_size()) {
    throw std::out_of_range("New size is too large");
  }
  if (size > capacity_) {
    vector<value_type> temp(size);
    for (size_type i = 0; i < size_; ++i) {
      temp.data_[i] = data_[i];
    }
    temp.size_ = size_;
    *this = std::move(temp);
  }
}

template <typename T>
typename vector<T>::size_type vector<T>::capacity() {
  return capacity_;
}

template <typename T>
void vector<T>::shrink_to_fit() {
  if (size_ < capacity_) {
    vector<value_type> temp(size_);
    for (size_type i = 0; i < size_; ++i) {
      temp.data_[i] = data_[i];
    }
    *this = std::move(temp);
  }
}

// modifiers

template <typename T>
void vector<T>::clear() {
  delete[] data_;
  data_ = nullptr;
  size_ = 0;
  // capacity_ = 0;
}

template <typename T>
typename vector<T>::iterator vector<T>::insert(iterator pos,
                                               const_reference value) {
  size_type index = pos - data_;
  if (size_ >= capacity_) {
    size_type new_capacity = capacity_ == 0 ? 1 : 2 * capacity_;
    reserve(new_capacity);
  }
  std::move_backward(data_ + index, data_ + size_, data_ + size_ + 1);
  data_[index] = value;
  ++size_;
  return data_ + index;
}

template <typename T>
void vector<T>::erase(iterator pos) {
  size_type index = pos - data_;
  std::move(data_ + index + 1, data_ + size_, data_ + index);
  --size_;
}

template <typename T>
void vector<T>::push_back(const_reference value) {
  if (size_ >= capacity_) {
    size_type new_capacity = capacity_ == 0 ? 1 : 2 * capacity_;
    reserve(new_capacity);
  }
  data_[size_] = value;
  ++size_;
}

template <typename T>
void vector<T>::pop_back() {
  if (size_ > 0) --size_;
}

template <typename T>
void vector<T>::swap(vector &other) {
  std::swap(data_, other.data_);
  std::swap(size_, other.size_);
  std::swap(capacity_, other.capacity_);
}

// insert_many

template <typename T>
template <typename... Args>
typename vector<T>::iterator vector<T>::insert_many(const_iterator pos,
                                                    Args &&... args) {
  vector<value_type> temp{args...};
  iterator cur_pos = begin() + (pos - begin());
  for (size_t i = 0; i < temp.size(); ++i) {
    cur_pos = insert(cur_pos, temp[i]);
    ++cur_pos;
  }
  return cur_pos;
}

template <typename T>
template <typename... Args>
void vector<T>::insert_many_back(Args &&... args) {
  insert_many(end(), args...);
}

};  // namespace s21

#endif
