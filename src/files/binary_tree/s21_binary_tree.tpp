#ifndef S21_BINARY_TREE_TPP
#define S21_BINARY_TREE_TPP

namespace s21 {

template <typename K, typename T>
BinaryTree<K, T>::Node::Node(K _key, T _value, Node* _parent)
    : left(nullptr),
      right(nullptr),
      parent(_parent),
      key(_key),
      value(_value) {}

template <typename K, typename T>
BinaryTree<K, T>::Node::~Node() {
  if (right) delete right;
  if (left) delete left;
}

template <typename K, typename T>
BinaryTree<K, T>::Iterator::Iterator() : ptr(nullptr) {}

template <typename K, typename T>
BinaryTree<K, T>::Iterator::Iterator(Node* _ptr) : ptr(_ptr) {}

template <typename K, typename T>
typename BinaryTree<K, T>::Iterator& BinaryTree<K, T>::Iterator::operator++() {
  if (ptr != nullptr) {
    if (ptr->right != nullptr) {
      ptr = ptr->right;
      if (ptr->left != nullptr)
        while (ptr->left != nullptr) ptr = ptr->left;
    } else if (ptr->parent != nullptr) {
      iterator tmp(ptr->parent);
      while ((tmp->parent != nullptr && ptr->key >= tmp->key) ||
             ptr->key >= tmp->key)
        tmp.ptr = tmp->parent;
      ptr = tmp.ptr;
    }
  }
  return *this;
}

template <typename K, typename T>
typename BinaryTree<K, T>::Iterator BinaryTree<K, T>::Iterator::operator++(
    int) {
  Iterator tmp(ptr);
  ++(*this);
  return tmp;
}

template <typename K, typename T>
typename BinaryTree<K, T>::Iterator& BinaryTree<K, T>::Iterator::operator--() {
  if (ptr != nullptr) {
    if (ptr->left != nullptr) {
      ptr = ptr->left;
      if (ptr->right != nullptr)
        while (ptr->right != nullptr) ptr = ptr->right;
    } else if (ptr->parent != nullptr) {
      iterator tmp(ptr->parent);
      while ((tmp->parent != nullptr && ptr->key < tmp->key) ||
             ptr->key < tmp->key)
        tmp.ptr = tmp->parent;
      ptr = tmp.ptr;
    }
  }
  return *this;
}

template <typename K, typename T>
typename BinaryTree<K, T>::Iterator BinaryTree<K, T>::Iterator::operator--(
    int) {
  Iterator tmp(ptr);
  --(*this);
  return tmp;
}

template <typename K, typename T>
bool BinaryTree<K, T>::Iterator::operator==(Iterator other) const {
  return other.ptr == this->ptr;
}

template <typename K, typename T>
bool BinaryTree<K, T>::Iterator::operator!=(Iterator other) const {
  return other.ptr != this->ptr;
}

template <typename K, typename T>
typename BinaryTree<K, T>::Node* BinaryTree<K, T>::Iterator::operator->()
    const {
  return ptr;
}

template <typename K, typename T>
BinaryTree<K, T>::BinaryTree() : size(0), fake(nullptr) {}

template <typename K, typename T>
BinaryTree<K, T>::BinaryTree(std::initializer_list<value_type> const& items)
    : size(0), fake(nullptr) {
  for (auto i = items.begin(); i != items.end(); ++i)
    insert(i->first, i->second);
}

template <typename K, typename T>
BinaryTree<K, T>::BinaryTree(const BinaryTree& other) : size(0), fake(nullptr) {
  assign(other);
}

template <typename K, typename T>
BinaryTree<K, T>::BinaryTree(BinaryTree&& other) : size(0), fake(nullptr) {
  assign(other);
  other.clear();
}

template <typename K, typename T>
BinaryTree<K, T>::~BinaryTree() {
  if (root.ptr) delete root.ptr;
}

template <typename K, typename T>
BinaryTree<K, T> BinaryTree<K, T>::operator=(BinaryTree&& other) {
  assign(other);
  other.clear();
  return *this;
}

template <typename K, typename T>
T& BinaryTree<K, T>::operator[](const K& key) {
  iter.ptr = root.ptr;
  Node* result = find(iter.ptr, key);
  if (result == nullptr) result = insert_or_assign(key, T()).first.ptr;
  return result->value;
}

template <typename K, typename T>
T& BinaryTree<K, T>::at(const K& key) {
  iter.ptr = root.ptr;
  Node* result = find(iter.ptr, key);
  if (result == nullptr) throw std::out_of_range("");
  return result->value;
}

template <typename K, typename T>
bool BinaryTree<K, T>::contains(Node* root, const K& key) {
  bool result = false;
  if (root != nullptr) {
    if (root->key == key)
      result = true;
    else if (root->key < key) {
      if (root->right != nullptr) result = contains(root->right, key);
    } else if (root->left != nullptr)
      result = contains(root->left, key);
  }
  return result;
}

template <typename K, typename T>
bool BinaryTree<K, T>::contains(const K& key) {
  return contains(root.ptr, key);
}

template <typename K, typename T>
typename BinaryTree<K, T>::iterator BinaryTree<K, T>::find(const K& key) {
  iter.ptr = root.ptr;
  iter.ptr = find(iter.ptr, key);
  return iter;
}

template <typename K, typename T>
std::pair<typename BinaryTree<K, T>::iterator, bool> BinaryTree<K, T>::insert(
    const value_type& value) {
  return insert(value.first, value.second);
}

template <typename K, typename T>
std::pair<typename BinaryTree<K, T>::iterator, bool> BinaryTree<K, T>::insert(
    const K& key, const T& obj) {
  bool result = false;
  std::pair<iterator, bool> pair;
  if (!contains(key)) {
    pair = insert_or_assign(key, obj);
    result = pair.second;
  }
  return std::pair<iterator, bool>(pair.first, result);
}

template <typename K, typename T>
bool BinaryTree<K, T>::empty() {
  return size == 0;
}

template <typename K, typename T>
std::pair<typename BinaryTree<K, T>::iterator, bool>
BinaryTree<K, T>::insert_or_assign(const K& key, const T& obj) {
  bool result = true;
  if (fake != nullptr && end_i.ptr != nullptr) {
    --end_i;
    end_i->right = nullptr;
    delete fake;
  }
  if (!empty()) iter.ptr = root.ptr;
  iter.ptr = insert(iter.ptr, key, obj);
  size++;
  set_begin();
  set_end();
  return std::pair<iterator, bool>(iter, result);
}

template <typename K, typename T>
typename BinaryTree<K, T>::Node* BinaryTree<K, T>::insert(Node* iter,
                                                          const K& key,
                                                          const T& value) {
  if (root.ptr == nullptr) {
    root.ptr = new Node(key, value, nullptr);
    iter = root.ptr;
  } else {
    if (iter->key == key) {
      iter->value = value;
    } else if (key > iter->key) {
      if (iter->right == nullptr) {
        iter->right = new Node(key, value, iter);
        iter = iter->right;
      } else
        iter = insert(iter->right, key, value);
    } else {
      if (iter->left == nullptr) {
        iter->left = new Node(key, value, iter);
        iter = iter->left;
      } else
        iter = insert(iter->left, key, value);
    }
  }
  return iter;
}

template <typename K, typename T>
void BinaryTree<K, T>::insert(Node* iter, Node* _node) {
  if (root.ptr == nullptr) {
    root.ptr = _node;
    iter = _node;
  } else if (_node->key >= iter->key) {
    if (iter->right == nullptr) {
      _node->parent = iter;
      iter->right = _node;
    } else
      insert(iter->right, _node);
  } else {
    if (iter->left == nullptr) {
      _node->parent = iter;
      iter->left = _node;
    } else
      insert(iter->left, _node);
  }
}

template <typename K, typename T>
typename BinaryTree<K, T>::iterator BinaryTree<K, T>::set_end() {
  end_i.ptr = root.ptr;
  if (root.ptr != nullptr) {
    while (end_i->right != nullptr) end_i.ptr = end_i->right;
    fake = new Node(end_i->key, end_i->value);
    fake->parent = end_i.ptr;
    end_i->right = fake;
    end_i.ptr = fake;
  }
  return end_i;
}

template <typename K, typename T>
typename BinaryTree<K, T>::iterator BinaryTree<K, T>::set_begin() {
  begin_i.ptr = root.ptr;
  if (root.ptr != nullptr)
    while (begin_i->left != nullptr) begin_i.ptr = begin_i->left;
  return begin_i;
}

template <typename K, typename T>
typename BinaryTree<K, T>::iterator BinaryTree<K, T>::begin() const {
  return begin_i;
}

template <typename K, typename T>
typename BinaryTree<K, T>::iterator BinaryTree<K, T>::end() const {
  return end_i;
}

template <typename K, typename T>
typename BinaryTree<K, T>::size_type BinaryTree<K, T>::Size() {
  return BinaryTree<K, T>::size;
}

template <typename K, typename T>
typename BinaryTree<K, T>::size_type BinaryTree<K, T>::max_size() {
  return std::numeric_limits<typename BinaryTree<K, T>::size_type>::max() /
         sizeof(typename BinaryTree<K, T>::value_type);
}

template <typename K, typename T>
void BinaryTree<K, T>::clear() {
  if (root.ptr) delete root.ptr;
  root.ptr = nullptr;
  fake = nullptr;
  iter.ptr = nullptr;
  begin_i.ptr = nullptr;
  end_i.ptr = nullptr;
  size = 0;
}

template <typename K, typename T>
void BinaryTree<K, T>::erase(iterator pos) {
  Node* tmp = nullptr;
  for (auto i = begin(); i != end(); ++i) {
    if (pos == i) {
      tmp = i.ptr;
    }
  }
  if (tmp != nullptr) {
    if (fake != nullptr) {
      --end_i;
      end_i->right = nullptr;
      delete fake;
    }
    if (tmp->parent == nullptr) {
      root.ptr = (tmp->right != nullptr) ? tmp->right : tmp->left;
    } else {
      iter.ptr = tmp->parent;
      if (iter->right == tmp)
        iter->right = nullptr;
      else
        iter->left = nullptr;
    }
    if (tmp->right != nullptr && root.ptr != tmp->right) {
      iter.ptr = root.ptr;
      insert(iter.ptr, tmp->right);
    }
    if (tmp->left != nullptr && root.ptr != tmp->left) {
      iter.ptr = root.ptr;
      insert(iter.ptr, tmp->left);
    }
    size--;
    tmp->right = nullptr;
    tmp->left = nullptr;
    delete tmp;
    set_begin();
    set_end();
  }
}

template <typename K, typename T>
void BinaryTree<K, T>::assign(const BinaryTree& other) {
  for (auto i = other.begin(); i != other.end(); ++i) insert(i->key, i->value);
}

template <typename K, typename T>
void BinaryTree<K, T>::swap(BinaryTree& other) {
  BinaryTree tmp(other);
  other.clear();
  other.assign(*this);
  BinaryTree<K, T>::clear();
  assign(tmp);
  tmp.clear();
}

template <typename K, typename T>
void BinaryTree<K, T>::merge(BinaryTree& other) {
  assign(other);
}

template <typename K, typename T>
typename BinaryTree<K, T>::Node* BinaryTree<K, T>::find(Node* root,
                                                        const K& key) {
  if (root != nullptr && root->key != key) {
    if (root->key < key) {
      if (root->right != nullptr)
        root = find(root->right, key);
      else
        root = nullptr;
    } else {
      if (root->left != nullptr)
        root = find(root->left, key);
      else
        root = nullptr;
    }
  }
  return root;
}

template <typename K, typename T>
typename BinaryTree<K, T>::iterator BinaryTree<K, T>::insert(const K& key) {
  if (fake != nullptr && end_i.ptr != nullptr) {
    --end_i;
    end_i->right = nullptr;
    delete fake;
  }
  if (!empty()) iter.ptr = root.ptr;
  insert(iter.ptr, new Node(key, K()));
  size++;
  set_begin();
  set_end();
  return iter;
}

template <typename K, typename T>
typename BinaryTree<K, T>::size_type BinaryTree<K, T>::count(const K& key) {
  size_type _count = 0;
  for (auto i = begin(); i != end(); ++i)
    if (key == i->key) _count++;
  return _count;
}

template <typename K, typename T>
std::pair<typename BinaryTree<K, T>::iterator,
          typename BinaryTree<K, T>::iterator>
BinaryTree<K, T>::equal_range(const K& key) {
  return std::pair<iterator, iterator>(lower_bound(key), upper_bound(key));
}

template <typename K, typename T>
typename BinaryTree<K, T>::iterator BinaryTree<K, T>::lower_bound(
    const K& key) {
  iter.ptr = nullptr;
  for (auto i = begin(); i != end(); ++i)
    if (key == i->key) {
      iter.ptr = i.ptr;
      break;
    }
  return iter;
}

template <typename K, typename T>
typename BinaryTree<K, T>::iterator BinaryTree<K, T>::upper_bound(
    const K& key) {
  iter.ptr = nullptr;
  auto i = end();
  while (i != begin()) {
    --i;
    if (key == i->key) {
      ++i;
      iter.ptr = i.ptr;
      break;
    }
  }
  return iter;
}

};  // namespace s21

#endif  // S21_BINARY_TREE_TPP