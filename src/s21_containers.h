#ifndef S21_CONTAINERS
#define S21_CONTAINERS

#include "files/list/s21_list.h"
#include "files/map/s21_map.h"
#include "files/queue/s21_queue.h"
#include "files/set/s21_set.h"
#include "files/stack/s21_stack.h"
#include "files/vector/s21_vector.h"

#endif
